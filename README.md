# RecommendationSystem

Recommendation System — Market Basket Analysis using Apriori Approach [Association Rule Mining] for a static number of products.

Technologies used:
- Python to build the ML model
- Flask to deploy the ML model
- HTML + CSS + Bootstrap for front end

![Image of Index Page](/images/index.PNG?raw=true "Index Page")

![Image of Buy Page](/images/buy.PNG?raw=true "Buy Products Page")

![Image of Recommendation](/images/recommendation.PNG?raw=true "Recommenations")

![Image of ARM rules](/images/rulesTable.PNG?raw=true "ARM Rules Page")

Steps to resolve the project:
- Create virtual environment
- Run the Flask App on localhost